from contextlib import contextmanager
from django.test import TestCase
from django.core import management
from django.core.files.images import ImageFile
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework.test import APITestCase
from wagtail.core.models import Page
from wagtail_cloudinary_image.models import CloudinaryImage
from .models import ExamplePage


@contextmanager
def test_cloudinary_image(*args, **kwds):
    image = CloudinaryImage(title="test")
    image.file = ImageFile(open("sandbox/wagtail.png", "rb"))
    image.save()
    try:
        yield image
    finally:
        # Delete the cloudinary image and ignore DB constraints
        try:
            image.file.delete()
        except IntegrityError:
            pass


class WagtailCloudinaryImagePageTestCase(TestCase):
    def test_image_in_template(self):
        home = Page.objects.last()

        with test_cloudinary_image() as image:
            example_page = ExamplePage(title="example", image=image)
            home.add_child(instance=example_page)
            example_page.save()

            res = self.client.get(example_page.url)
        self.assertContains(res, "h_125,w_150")


class WagtailCloudinaryAPITestCase(APITestCase):
    def test_image_in_template(self):
        home = Page.objects.last()

        with test_cloudinary_image() as image:
            example_page = ExamplePage(title="example", image=image)
            home.add_child(instance=example_page)
            example_page.save()

            url = reverse("wagtailapi:pages:detail", args=[example_page.pk])
            res = self.client.get(url)
        self.assertContains(res, "media/original_images/sandboxwagtail")
